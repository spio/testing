# Testowanie oprogramowania

Przedmiot stanowi wprowadzenie do podstawowych pojęć, technik i narzędzi związanych z procesem testowania oprogramowania. Zakres obejmuje scenariusze testowania oraz planowanie, techniki statyczne, testy jednostkowe, TDD, wybrane narzędzia i konkretne technologie programistyczne, jakość testów.

## Wymagania wstępne

Przedmiot jest przygotowany dla uczestników o różnym poziomie znajomości technologii informatycznych. Celem swobodnego poruszania się po tematach najlepiej znać w stopniu podstawowym chociaż jeden język programowania, jedno środowisko programistyczne oraz znać podstawowe pojęcia i komendy systemu wersjonowania kodu git.

## Zasady zaliczenia

Podstawą zaliczenia jest realizacja zadań oraz obecność.

## Kontakt

pawel.martenka[@]cs.put.poznan.pl

## Lista tematów

 - 00 Wykład https://gitlab.com/spio/00lecture
 - 01 Wprowadzenie https://gitlab.com/spio/01intro
 - 02 Planowanie https://gitlab.com/spio/02plan
 - 03 Techniki statyczne https://gitlab.com/spio/03static
 - 04 Testy jednostkowe https://gitlab.com/spio/04unit
 - 05 TDD https://gitlab.com/spio/05tdd
 - 06 Obiekty zastępcze i narzędzia https://gitlab.com/spio/06tool
 - 07 Testowanie API https://gitlab.com/spio/07web
 - 08 Jakość testów https://gitlab.com/spio/08quality


